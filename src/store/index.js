import Vue from "vue";
import Vuex from "vuex";
import API from "@/services/api";

Vue.use(Vuex);

export default new Vuex.Store({
  actions: {
    logout({ commit }) {
      return new Promise((resolve) => {
        commit("logout");
        localStorage.removeItem("token");
        localStorage.removeItem("userId");
        delete API.defaults.headers.common["Authorization"];
        resolve();
      });
    },
  },
});
