import axios from "axios";
import store from "../store/index";
import router from "../router/index";

var instance = axios.create({
  baseURL: process.env.VUE_APP_BASEURL,
});

instance.interceptors.request.use(function(config) {
  const token = localStorage.getItem("token");
  config.headers.common["Authorization"] = `${token}`;
  return config;
});

instance.interceptors.response.use((response) => {
  if (
    response.data &&
    response.data == "authentication required for this call"
  ) {
    store.dispatch("logout").then(() => {
      router.push("/");
    });
  }
  return response;
});

export default instance;
