import Vue from "vue";
import Router from "vue-router";

import LandingPage from "@/views/LandingPage.vue";
import Employee from "@/views/Employee.vue";

Vue.use(Router);

const routes = [
  {
    path: "/",
    name: "login",
    component: LandingPage,
    meta: {
      layout: "login",
    },
  },
  {
    path: "/dashboard",
    name: "employee",
    component: Employee,
  },
  {
    path: "/employees",
    name: "employee",
    component: Employee,
  },
];

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
