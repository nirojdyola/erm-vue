import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import Default from "@/layouts/Default.vue";
import Login from "@/layouts/Login.vue";
import moment from "moment";
import store from "./store";

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

Vue.component("default-layout", Default);
Vue.component("login-layout", Login);
Vue.prototype.moment = moment;

Vue.config.productionTip = false; // prevent development warning

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
