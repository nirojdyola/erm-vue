# frontend-vue

## Project setup
```
mv .env.example .env
```

Change the `VUE_APP_BASEURL` where backend is running. For example:

VUE_APP_BASEURL="http://localhost:3011/api/"

where "http://localhost:3011/api/" is the one where backend server is running.

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
